# ModPack Rebels-Games.com

###### **`Servers Mods Collection`** http://steamcommunity.com/workshop/filedetails/?id=874530613

## Mods in ModPack: 

* `Rebels Central Planet + Other` - http://steamcommunity.com/sharedfiles/filedetails/?id=1177844421

* `Rebels Admin Block` - http://steamcommunity.com/sharedfiles/filedetails/?id=1223401164

* `Rebels Advanced Blocks & Items & Ores` - http://steamcommunity.com/sharedfiles/filedetails/?id=1177859564

* `Rebels Alpha DeadSun Planet` - http://steamcommunity.com/sharedfiles/filedetails/?id=1177847453

* `Rebels Alpha Planets + Hud + Skybox` - http://steamcommunity.com/sharedfiles/filedetails/?id=1177850004

* `Rebels Beta Planets + Hud + Skybox` - http://steamcommunity.com/sharedfiles/filedetails/?id=1177850977

* `Rebels Blocks & Items & Ores` - http://steamcommunity.com/sharedfiles/filedetails/?id=1177860728

* `Rebels Build Info` - http://steamcommunity.com/sharedfiles/filedetails/?id=1177857292

* `Rebels Delta Planets + Hud + Skybox` - http://steamcommunity.com/sharedfiles/filedetails/?id=1177853243

* `Rebels Economy & Trade` - http://steamcommunity.com/sharedfiles/filedetails/?id=1177858833

* `Rebels Energy Shield` - http://steamcommunity.com/sharedfiles/filedetails/?id=1177856639

* `Rebels Gamma Planets + Hud + Skybox` - http://steamcommunity.com/sharedfiles/filedetails/?id=1177854751

* `Rebels NPC` - http://steamcommunity.com/sharedfiles/filedetails/?id=1184749798

* `Rebels Respawn Ships` - http://steamcommunity.com/sharedfiles/filedetails/?id=1177857972

* `Rebels Weapons` - http://steamcommunity.com/sharedfiles/filedetails/?id=1177855711

### Orginal Author's some used mods:

- *Build Info - extra block information* - `Digi` - http://steamcommunity.com/sharedfiles/filedetails/?id=514062285

- *Message Play* - `Chrisbot` - https://steamcommunity.com/sharedfiles/filedetails/?id=960943569

- *VCZ Doors* - `Vicizlat` - https://steamcommunity.com/sharedfiles/filedetails/?id=856352904

- *Energy Shields* - `[XpL] Cython` - http://steamcommunity.com/sharedfiles/filedetails/?id=484504816

- *Battle Cannon and Turrets (DX11)* - `Rider` - http://steamcommunity.com/sharedfiles/filedetails/?id=543635673