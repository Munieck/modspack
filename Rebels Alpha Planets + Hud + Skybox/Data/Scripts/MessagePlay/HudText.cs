﻿using System;
using Sandbox.ModAPI;
using VRage.Game.Components;

[MySessionComponentDescriptor(MyUpdateOrder.BeforeSimulation)]
public class HudText : MySessionComponentBase
{
    bool initialize = true;

    public override void UpdateBeforeSimulation()
    {
        if (MyAPIGateway.Utilities.IsDedicated && MyAPIGateway.Multiplayer.IsServer) return;
        if (MyAPIGateway.Session == null) return;
        if (initialize)
        {
            MyAPIGateway.Utilities.GetObjectiveLine().Title = DateTime.Now.ToShortTimeString();
            MyAPIGateway.Utilities.GetObjectiveLine().Objectives.Add(null);
            MyAPIGateway.Utilities.GetObjectiveLine().Show();
            base.UpdateAfterSimulation();
        }
    }
}